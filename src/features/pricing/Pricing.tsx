import React from 'react';
import { useTranslation } from 'react-i18next';
import { PureQueryOptions } from 'apollo-client';

import styles from './pricing.module.scss';
import PageTitle from '../../common/pageTitle/PageTitle';
import BerthPricing, { BerthPricingProps } from './berthPricing/BerthPricing';
import WinterStoragePricing, { WinterStoragePricingProps } from './winterStoragePricing/WinterStoragePricing';
import HarborServicePricing, { HarborServicePricingProps } from './harborServicePricing/HarborServicePricing';
import AdditionalServicePricing, {
  AdditionalServicePricingProps,
} from './additionalServicePricing/AdditionalServicePricing';
import PageContent from '../../common/pageContent/PageContent';

export interface PricingProps {
  berthsData: BerthPricingProps['data'];
  winterStorageData: WinterStoragePricingProps['data'];
  harborServicesData: HarborServicePricingProps['data'];
  additionalServicesData: AdditionalServicePricingProps['data'];
  additionalServicesModal: Pick<
    AdditionalServicePricingProps,
    'isModalOpen' | 'onAddServiceClick' | 'onEditRowClick' | 'onCloseModal' | 'editingServiceId' | 'onSubmitForm'
  >;
  loading: boolean;
  refetchQueries?: PureQueryOptions[] | string[];
}

const Pricing = ({
  berthsData,
  winterStorageData,
  harborServicesData,
  additionalServicesData,
  additionalServicesModal,
  loading,
  refetchQueries,
}: PricingProps) => {
  const { t } = useTranslation();

  return (
    <PageContent className={styles.pricing}>
      <PageTitle title={t('pricing.title')} />
      <BerthPricing data={berthsData} loading={loading} refetchQueries={refetchQueries} />
      <WinterStoragePricing data={winterStorageData} loading={loading} />
      <HarborServicePricing data={harborServicesData} loading={loading} />
      <AdditionalServicePricing {...additionalServicesModal} data={additionalServicesData} loading={loading} />
    </PageContent>
  );
};

export default Pricing;
